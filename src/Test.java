
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

class OneThread extends Thread{
  public void run(){
    while(true){ 
		DateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
        System.out.println("Thread A:"+dateFormat.format(cal.getTime())); 
        try{ 
            Thread.sleep(3000); 
        }catch(Exception e){} 
    }
  }
} 

class TwoThread extends Thread{
  public void run(){
    while(true){ 
		DateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
        System.out.println("Thread B:"+dateFormat.format(cal.getTime())); 
        try{ 
            Thread.sleep(5000); 
        }catch(Exception e){} 
    }
  }
}

class ThreeThread extends Thread{
  public void run(){
    while(true){
		DateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
		Date date = new Date();
		Calendar cal = Calendar.getInstance();	
        System.out.println("Thread C:"+dateFormat.format(cal.getTime())); 
        try{ 
            Thread.sleep(7000); 
        }catch(Exception e){} 
    }
  }
}

public class Test{
  public static void main(String[] args){
	
    OneThread t1 = new OneThread();
	TwoThread t2 = new TwoThread();
	ThreeThread t3 = new ThreeThread();
	
    t1.start();
    t2.start();
	t3.start(); 
  }
}

